export class Neuron {
  nEntradas: number;
  nSalidas: number;
  nPatrones: number;
  datos: number[][];
  salidaSoma: number[];
  newMatrizPesos: number[][];
}
