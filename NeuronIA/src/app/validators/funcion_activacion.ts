import { AbstractControl, ValidatorFn, Validators } from "@angular/forms";

export function seleccionarOpcion(): ValidatorFn {
  return (control: AbstractControl) => {
    const value = <string>control.value;
    if (!value) return;
    if (value.length == 0) return;

    if (value === "Seleccionar...") {
      return {
        seleccionarOpcion: {
          message: 'debe seleccionar una opcion'
        }
      };
    }
    return;
  }
}
