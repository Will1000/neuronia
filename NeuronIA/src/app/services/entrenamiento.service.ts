import { Injectable } from '@angular/core';
import { Entrenamiento } from '../models/entrenamiento';
import { Neuron } from '../models/neuron';

@Injectable({
  providedIn: 'root'
})
export class EntrenamientoService {

  constructor() { }


  calcularErrorERM(errorPatron: number[], nPatrones: number): number {
    let errorRMS: number = 0;

   console.log(errorPatron);
    for (let i = 0; i < errorPatron.length; i++) {
      errorRMS += errorPatron[i] / nPatrones;
    }

    console.log("Error RMS: "+errorRMS);
    return errorRMS;

  }

  ajustarPesos(patron: number, neurona: Neuron, matrizPesos: number[][], entrenamiento: Entrenamiento) {
    let row: number[] = [];
    let x: number[] = []; // patron
    let newMatrizPeso: number[][] = [];

    for (let i = 0; i < neurona.nEntradas; i++) {
      x = neurona.datos[patron];
      row = [];
      for (let j = 0; j < neurona.nSalidas; j++) {

        console.log("W"+(j+1)+""+(i+1)+": "+ matrizPesos[i][j]+" + "+entrenamiento.rAprendizaje+" * "+entrenamiento.errorLineal[j]+" * "+ x[i]);

        row.push((matrizPesos[i][j])+ (entrenamiento.rAprendizaje* entrenamiento.errorLineal[j] * x[i]));
      }
      newMatrizPeso.push(row);
    }

    matrizPesos = [];

    matrizPesos = newMatrizPeso;

    console.log("Nueva matriz de pesos: ");
    console.log(matrizPesos);

    return matrizPesos;
  }

  ajustarUmbrales(nSalidas: number, umbrales: number[], entrenamiento: Entrenamiento) {
    let newUmbrales: number[] = [];
    let x0: number = 1;

    for (let i = 0; i < nSalidas; i++) {

      newUmbrales.push(umbrales[i] + (entrenamiento.rAprendizaje * entrenamiento.errorLineal[i] * x0));
    }

    return newUmbrales;
  }

  salidaRed(salida: number, patron: number, fActivacion: string, datos: number[][]): number {

    if (fActivacion === "Escalon") {
      if (salida >= 0) return 1;
      if (salida < 0) return 0;
    } else if (fActivacion == 'Rampa') {
      if (salida < 0) return 0;
      if (salida >= 0 && salida <= 1) return datos[patron][0];
      if (salida > 1) return 1;
    }else {
      return salida;
    }

  }

  obtenersalidas(patron: number[], nEntradas: number): number[] {
    let salidas: number[] = [];
    for (let i = nEntradas; i < patron.length; i++) {
      salidas.push(patron[i]);
    }
    console.log("Salidas (Yd1, Yd2): "); console.log(salidas);
    return salidas;
  }

  inicializarPesos(neurona: Neuron): number[][] {

    let matrizPesos: number[][] = [];

    let row: number[] = [];
    for (let i = 0; i < neurona.nEntradas; i++) {
      row = [];
      for (let j = 0; j < neurona.nSalidas; j++) {
        row.push(parseFloat((Math.random() * 1).toFixed(5)));
      }
      matrizPesos.push(row);
    }

    return matrizPesos;
  }

  inicializarUnbrales(nSalidas: number): number[] {
    let umbrales: number[] = [];

    // for (let i = 0; i < nSalidas; i++) {
    //   umbrales.push(parseFloat((Math.random() * 1).toFixed(5)));
    // }
    // return umbrales;

    umbrales.push(0.031180801350241927);

    return umbrales;
  }



}
