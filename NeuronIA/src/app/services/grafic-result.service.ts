import { Injectable } from '@angular/core';

interface ResultGrfic {
  name: string;
  value: number
}

@Injectable({
  providedIn: 'root'
})
export class GraficResultService {

  private data = [
    {
      name: 'Error RMS vs Iteraciones',
      series: [],
    },
  ];

  private dataGrafic = [
    {
      name: 'YR',
      series: [
      ],
    },
    {
      name: 'YD',
      series: [
      ],
    },
  ];

  get resultGrafic() {
    return this.data;
  }

  get resultGraficSimu() {
    return this.dataGrafic;
  }

 addGraficSimu(dataGrafic: any) {
   this.dataGrafic= dataGrafic;
 }

  addGrafic(dataGrafic: any) {
    this.data = dataGrafic;
  }
}
